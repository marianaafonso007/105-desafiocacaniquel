package com.company;

import sun.misc.FloatingDecimal;

import java.util.*;

public class CacaNiquel extends Jogo {

    private List<Slots> resultadoSlots;
    private final int quantidadeSlot = 3;

    public void sorteio(){
        Random random = new Random();
        resultadoSlots = new ArrayList<>();
        for (int i = 0; i < quantidadeSlot; i++){
            this.resultadoSlots.add(Slots.values()[random.nextInt(Slots.values().length)]);
        }
        System.out.println(resultadoSlots);
    }

    public void ValidacaoSlots(){
        valorPartida();
        boolean todosIgual = resultadoSlots.stream().distinct().limit(resultadoSlots.size()).count() == 1;
        if (!todosIgual){
            somarResultado(this.valorPartida);
        }
        else {
            this.valorPartida = this.valorPartida * 100;
            somarResultado(this.valorPartida);
        }
    }

    public void valorPartida(){
        this.valorPartida = 0.00;
        for (Slots resultado: this.resultadoSlots){
            this.valorPartida += resultado.getValor();
        }
    }

    @Override
    public void somarResultado(double valorPartida) {
        this.resultado += valorPartida;
    }
}
