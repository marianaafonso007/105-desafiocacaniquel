package com.company;

import java.util.Scanner;

public class UI {

    Scanner leituraDados = new Scanner(System.in);
    public static void mensagemUser(String mensagem){
        System.out.println(mensagem);
    }
    private boolean iniciarjogo;
    CacaNiquel cacaNiquel = new CacaNiquel();

    public void inicializacao() {

        do {
            mensagemUser("Bem vindo ao jogo Caça Níquel, digite 1 para jogar ou 2 para sair: ");
            int entrada = leituraDados.nextInt();
            if (entrada > 0) {
                cacaNiquel.sorteio();
                cacaNiquel.ValidacaoSlots();
                mensagemUser("Total obtido na partida: " + cacaNiquel.valorPartida + " Resultado do jogo:" +  cacaNiquel.resultado);
            } else {
                return;
            }
            mensagemUser("Deseja continuar jogando? Digite 1 para SIM e 2 para NÃO!");
            entrada = leituraDados.nextInt();
            iniciarjogo = entrada != 1 ? false : true;
        } while (iniciarjogo);
    }


}

