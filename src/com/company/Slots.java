package com.company;

import java.util.HashMap;

public enum Slots {
    BANANA(10),
    FRAMBOESA(50),
    MOEDA(100),
    SETE(300),
    COPAS(530);

    public double valor;

    public double getValor() {
        return valor;
    }
    Slots(int valor){
        this.valor = valor;
    }
}
